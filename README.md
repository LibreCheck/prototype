# LibreCheck PHP Backend
The PHP file manager system for LibreCheck.

```
git clone https://gitlab.com/librecheck/php-backend
cd php-backend
php -S 127.0.0.1:8100
```

Also include this basic file with random info as a people.csv file in the main directory:

```
groupid,personid,first,last,guardfirst,guardlast,dob,gender,medical
2345678901,1,Ted,Jameson,John,Jameson,2007-03-17,male,none
2345678901,2,Amy,Jameson,John,Jameson,2010-04-09,female,none
3345678901,3,Other,Person,Some,Person,2003-03-12,male,none
3345678901,4,Other,Guy,Some,Person,2003-03-12,male,none
```

Include this as `places.txt` in the `config/` directory:

```
Test place
Another place
```